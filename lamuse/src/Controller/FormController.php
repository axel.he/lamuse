<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Formulaire;
use App\Form\AddImageType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;




class FormController extends AbstractController
{
    #[Route('/form', name: 'app_form')]
    public function add(Request $request, EntityManagerInterface $em, SluggerInterface $slugger): Response
    {
        $img = new Formulaire();

        $form = $this->createForm(AddImageType::class, $img);

        $form->handleRequest($request);
        
        if($form -> isSubmitted() && $form -> isValid()){
            $des = $img -> getDescription();
            $img -> setDescription($des);
            $image = $img -> getImage();
            $img -> setImage($image);

            $em -> persist($img);
            $em -> flush();

            return $this->redirectToRoute('app_main');
        }

        return $this->render('form/index.html.twig', [
            'form' => $form -> createView()
        ]);
    }

    
}
